/**********************************************
* File: detect_cycle.cpp
* Author: Dominic Marques
* Email: dmarque4@nd.edu
*
* Contains an STL implementation of an Adjacency List
* graph Depth-First Search (no directions or weights) using C++ STL Vector
* searches this graph for a cycle
*
mmorri22@remote303:~/testUserNDCSE/inclasswork/GraphSTL$ g++ -g -std=c++11 -Wpedantic detect_cycle.cpp -o cycle
mmorri22@remote303:~/testUserNDCSE/inclasswork/GraphSTL$ ./cycle

Output may be found at cycle.out. Can be generated using ./cycle > cycle.out

**********************************************/

#include <iostream>
#include <vector>
#include <unordered_map>
#include <stack>
#include <fstream>

#define ROOT 0

// data structure to store graph edges
template<class T>
struct Edge {
	T src;
	T dest;
	int weight;

   template <typename U>      // all instantiations of this template are friends
   /********************************************
   * Function Name  : operator<<
   * Pre-conditions :  std::ostream&, const Edge<U>&
   * Post-conditions: friend std::ostream&
   * Overloaded Friend Operator<< to print an edge
   ********************************************/
   friend std::ostream& operator<<( std::ostream&, const Edge<U>& );

};

template <typename T>
std::ostream& operator<<( std::ostream& os, const Edge<T>& theEdge) {

   std::cout << "{" << theEdge.src << " " << theEdge.dest << " " << theEdge.weight << "} ";

   return os;
}


template<class T>
struct Vertex {
	T value;
	mutable std::vector< Edge <T> > edges;

	/********************************************
	* Function Name  : operator==
	* Pre-conditions : const Vertex<T>& rhs
	* Post-conditions: bool
	*
	* Overloaded == operator to compare vertex value
	********************************************/
	bool operator==(const Vertex<T>& rhs) const{

		return value == rhs.value;

	}

	/********************************************
	* Function Name  : operator<
	* Pre-conditions : const Vertex<T>& rhs
	* Post-conditions: bool
	*
	* Overloaded < operator to compare vertex values
	********************************************/
	bool operator<(const Vertex<T>& rhs) const{

		return value < rhs.value;

	}
};

// class to represent a graph object
template<class T>
class Graph
{
	public:
		// construct a vectors to represent an adjacency list
		std::vector< Vertex<T> > adjList;

		// Hash Table to correlate Verticies with location in vector
		std::unordered_map< T, int > hashVerts;

		/********************************************
		* Function Name  : Graph
		* Pre-conditions : const std::vector< Edge<T> > &edges
		* Post-conditions: none
		*
		* Graph Constructor
		********************************************/
		Graph(std::vector< Edge<T> > &edges, bool directional)
		{

			// add edges to the directed graph
			for (auto &edge: edges)
			{

				// Insert Origin
				addEdge(edge);

				// Since this is undirected, put in opposite
				// Uncomment if graph is directional
				// Create a temp edge to reverse origin and destination
				if(!directional){
					Edge<T> tempEdge = {edge.dest, edge.src, edge.weight};
					addEdge(tempEdge);
				}

			}
		}

		/********************************************
		* Function Name  : addEdge
		* Pre-conditions : Edge<T> edge
		* Post-conditions: none
		*
		* Insert an edge into the graph
		********************************************/
		void addEdge(const Edge<T>& edge){

			// Create a temporary vertex with the source
			Vertex<T> tempVert;
			tempVert.value = edge.src;

			// Element was not found
			if(hashVerts.count(tempVert.value) == 0){

				// HashWords.insert( {wordIn, 1} );
				hashVerts.insert( {tempVert.value, adjList.size()} );

				// Inset the edge into the temporary vertex
				// Serves as the first edge
				tempVert.edges.push_back(edge);

				// Insert the vertex into the set
				adjList.push_back(tempVert);

			}
			// Element was found!
			else{

				// Use the hash to get the location in adjList, then push onto the edges vector
				adjList.at(hashVerts[tempVert.value]).edges.push_back(edge);

			}
		}

		/********************************************
		* Function Name  : returnHashLocation
		* Pre-conditions : T value
		* Post-conditions: int
		*
		* Returns the location in the graph of the requested value
		********************************************/
		// In Class Code Goes Here
		int returnHashLocation(T value){

			return hashVerts[value];
		}

};

/********************************************
* Function Name  : printGraph
* Pre-conditions : const Graph<T>& graph
* Post-conditions: none
*
* Prints all the elements in the graph
********************************************/
template<typename T>
void printGraph(const Graph<T>& graph){

	std::cout << "Number of buckets is: " << graph.adjList.size() << std::endl;
	std::cout << "Origin: {Destination, Weight}" << std::endl;

	for(int iter = 0; iter < graph.adjList.size(); iter++){

		std::cout << graph.adjList.at(iter).value << ": ";

		for(int jter = 0; jter < graph.adjList.at(iter).edges.size(); jter++){

			std::cout << graph.adjList.at(iter).edges.at(jter);

		}

		std::cout << std::endl;
	}

	std::cout << std::endl;

}

/********************************************
* Function Name  : testCycle
* Pre-conditions : Graph<T>& graph, int vecLoc, std::stack<T>& DFSOrder,
*				   std::unordered_map<T, bool>& visitedVerts
* Post-conditions: none
*
********************************************/
template<class T>
bool testCycle(Graph<T>& graph, int vecLoc, std::stack< T >& DFSOrder,
	std::unordered_map<T, bool>& visitedVerts){

		// Mark the root node as visited
		visitedVerts.insert( { graph.adjList.at(vecLoc).value, true } );

		// Get the vertex based on vecLoc
		Vertex<T>* tempVert = &graph.adjList.at(vecLoc);


		for(int iter = 0; iter < tempVert->edges.size(); iter++){

			T tempDest = tempVert->edges.at(iter).dest;

			// First, check if the destination is in the Hash Table
			// If not, that means its a leaf in a directed graph.
			// Failed the previous check, so it is not the sought destination
			// Return false
			if(graph.hashVerts.count( tempDest ) == 0){
				return false;
			}

			// Otherwise, the path continues
			if( visitedVerts.count( tempDest ) == 0 ){

				// Mark the destination vertex from the edge as visited
				visitedVerts.insert( { tempDest, true } );

				// TODO: fix recursive call
				// The recursive call found the node, then push current destination onto the stack
				return testCycle(graph, graph.returnHashLocation(tempDest), DFSOrder, visitedVerts);
			} else {
				std::cout << "Cycle detected. Erasing " << tempVert->edges.at(iter) << std::endl;
				tempVert->edges.erase((tempVert->edges).begin() + iter);
				return true;
			}
		}

		return false;

	}

/********************************************
* Function Name  : testCycle
* Pre-conditions : Graph<T>& graph, std::stack<T>& DFSOrder, T& findElem
* Post-conditions: none
*
* Recursive basic function for DFS Traversal
* Stores traversal results in DFSOrder
********************************************/
template<class T>
void testCycle(Graph<T>& graph, std::stack< T >& DFSOrder){

	// If the Graph has no elements, the return false
	if(graph.adjList.size() == 0)
		return;

	// Create a Hash for visited Vertices
	std::unordered_map<T, bool> visitedVerts;

	// If this returns true, then the element was found, add the root
	if(!testCycle(graph, ROOT, DFSOrder, visitedVerts)){

		DFSOrder.push(graph.adjList.at(0).value);
	}

}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main Driver Function
********************************************/
int main(int argc, char** argv)
{
	// Allocate the edges as shown in the description
	std::vector< Edge<char> > edges =
	{
		{'N', 'O', 1}, {'N', 'T', 1}, {'O', 'R', 1},
		{'O', 'E', 1}, {'T', 'R', 1}, {'R', 'D', 1},
		{'E', 'D', 1}
	};

	std::cout << "-------------------------------------" << std::endl;
	std::cout << "Run NOTRED Test Cases with directed edges" << std::endl << std::endl;


	// construct graph with directed edges
	Graph<char> charGraph(edges, true);

	// Test the character Graph DFS
	std::stack<char> charDFSOrder;
	printGraph(charGraph);
	std::cout << '\n';
	testCycle(charGraph, charDFSOrder);
	std::cout << '\n';
	printGraph(charGraph);

	// Allocate the edges as shown in the description
	std::vector< Edge<int> > edgeInts =
	{
		{0, 1, 1}, {0, 4, 1},
		{1, 4, 1}, {1, 2, 1}, {1, 3, 1},
		{2, 3, 1},
		{3, 4, 1}
	};
	std::cout << "-------------------------------------" << std::endl;

	std::cout << "Run Classic Test Cases with directed edges" << std::endl << std::endl;

	// construct graph without directed edges
	Graph<int> intGraph(edgeInts, true);
	std::stack<int> intDFSOrder;
	printGraph(intGraph);
	std::cout << '\n';
	testCycle(intGraph, intDFSOrder);
	std::cout << '\n';
	printGraph(intGraph);

	// Allocate the edges as shown in the description
	std::vector< Edge<int> > edgeIntsCycle =
	{
		{0, 1, 1},
		{1, 4, 1}, {1, 2, 1}, {1, 3, 1},
		{2, 3, 1},
		{3, 4, 1},
		{4, 0, 1}
	};
	std::cout << "-------------------------------------" << std::endl;

	std::cout << "Run Classic Test Cases with directed edges and cycle" << std::endl << std::endl;

	// construct graph without directed edges
	Graph<int> intGraphCycle(edgeIntsCycle, true);
	std::stack<int> intDFSOrderCycle;
	printGraph(intGraphCycle);
	// std::cout << '\n';
	testCycle(intGraphCycle, intDFSOrderCycle);
	std::cout << '\n';
	printGraph(intGraphCycle);



	return 0;
}
