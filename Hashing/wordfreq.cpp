# include <map>
# include <iterator>
# include <string>
# include <iostream>
# include <fstream>

int main(int argc, char const *argv[]) {
    // read file into map
    std::map<std::string, int> words;
    std::ifstream file;


    // fileIO from https://stackoverflow.com/questions/20372661/read-word-by-word-from-file-in-c
    if (argc > 1) {
        file.open(argv[1]);
        if (!file.is_open()) return 1;
    }

    std::string word;
    while (file >> word) {
        if (words.find(word) != words.end()) {
            words[word]++;
        } else {
            words.insert(std::map<std::string,int>::value_type(word,1));
        }
    }

    std::map<std::string, int>::iterator iterOr;
	std::cout << "The word counts are: " << std::endl;

	for (iterOr = words.begin(); iterOr != words.end(); iterOr++) {
		std::cout << iterOr->first << ": " << iterOr->second << std::endl;
	}


    return 0;
}
