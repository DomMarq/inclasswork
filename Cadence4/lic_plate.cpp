# include <unordered_map>
# include <vector>
# include <string>
# include <fstream>
# include <cctype>
# include <iostream>
# include <set>
# include <algorithm>

// Compilation: g++ -g -Wpedantic -std=c++11 lic_plate.cpp -o lic_plate

void make_dictionary(std::unordered_map<std::string, int> &dict) {
    std::ifstream file;
    std::string temp;
    file.open("words.txt");  // words.txt is a copy of /usr/share/dict/words

    while (file >> temp) {
        for (size_t i = 0; i < temp.size(); i++) {
            temp[i] = tolower(temp[i]);
        }
        dict[temp] = 5;

    }

    file.close();

}

std::string get_letters(std::string plate) {
    std::string letters = "";
    for (size_t i = 0; i < plate.size(); i++) {
        if (isalpha(plate[i])) {
            letters.push_back(tolower(plate[i]));
        }
    }

    return letters;
}

void permute(std::string letters, std::set<std::string> &perms) {
    std::string temp = letters;
    if (letters.size() == 1) {
        perms.emplace(letters);
    }
    for (size_t i = 0; i < letters.size(); i++) {
        std::string c(1, letters[i]);
        temp = letters;
        temp.erase(temp.begin() + i);
        permute(temp, perms);
    }
    for (size_t i = 0; i < letters.size(); i++) {
        std::rotate(letters.begin(), letters.begin() + 1, letters.end());
        perms.emplace(letters);
    }
}


int main(int argc, char const *argv[]) {
    std::unordered_map<std::string, int> dict;
    make_dictionary(dict);

    std::vector<std::string> plates = {"RT 123SO", "RC 1001A"};
    std::string letters;
    for (size_t i = 0; i < plates.size(); i++) {
        std::set<std::string> perms;
        letters = get_letters(plates[i]);
        // std::cout << letters << std::endl;
        permute(letters, perms);
        for (auto perm : perms) {
            if (dict[perm] == 5) {
                std::cout << perm << std::endl;
                break;
            }

        }
    }

    return 0;
}
