/**********************************************
* File: BookFreq.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This is a solution to the Word Frequency question 
* Compilation command: g++ -g -std=c++11 -Wpedantic BookFreq.cpp -o BookFreq
* Run: ./BookFreq Shakespeare.txt
**********************************************/
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>
#include <iterator>

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver function for the program  
********************************************/
int main(int argc, char** argv){

	// Create a HashMap using a list of integers as the values
	// The list of values represents the location in the array
	std::unordered_map< std::string, int > HashWords;
	
	// Note, if you wanted them in order, you'd use std::map< std::string, int>
	// But this uses an AVL Tree, so you lose O(1) insert and find.
	
	// Get the inputstream
	std::ifstream bookFile;
	bookFile.open(argv[1]);
	if(!bookFile.is_open()){
		std::cout << "The file " << argv[1] << " does not exist. Exiting Program..." << std::endl;
		exit(-1);
	}
	
	// Get the strings and put in the Hash Table
	std::string wordIn;
	while (bookFile >> wordIn)
	{
		// If the word has not yet been hashed
		if(HashWords.count(wordIn) == 0){
			
			// Put the word in, and set the count to 1
			HashWords.insert( {wordIn, 1} );
		}
		else{
			// Increment the count 
			HashWords[wordIn]++;
		}

	}

	// Close the ifstream
	bookFile.close();
	
	// Iterate through the Hash and print the words and their count 
	std::cout << "The count of the words of the works of Shakespeare:" << std::endl;
	
	for(std::unordered_map<std::string, int>::iterator iter = HashWords.begin(); iter != HashWords.end(); iter++){
		std::cout << iter->first << " " << iter->second << std::endl;
	}

	return 0;

}

