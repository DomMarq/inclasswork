/************************************
 * File Name: Lecture0-B.cc  
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu
 *
 * This program teaches students about do-while loops,
 * pointer and memory allocation, and input parsing protection
 * 
 * Modified for C++, including libraries, and std namespace
 * All previous C code noted with   // PREVIOUS C code
 * New C++ code noted with          // EQUIVALENT C++ code
 *
 * This code is a modified version of the EX0206do.c function
 * provided by Cadence Design Systems, written by mikep
 *************************************/

// Contains the main functions
#include "Lecture0-B.h"

/********************************
 * Function: main
 * Postconditions: int, char **
 * Postconditions: int
 *
 * This is the main driver function for the program
 ********************************/
int main(int argc, char **argv) {
    
	// Create character strings
    char *str, *str2;
	// Set the length to be a character and a null character = 2
    const size_t length = 2;
	
	// Allocate the memory for str and str2
    allocateStrMemory(&str, length);
    allocateStrMemory(&str2, length);
	
	// Print the string to the user
    printStrings(str, str2);
    
	// Unsigned int to count the number of iterations
    unsigned iter=0;
    
    do {
        // PREVIOUS C code
        // fprintf(stdout, "%10d : New iteration? (y/n) [n] : ", iter);
        // EQUIVALENT C++ code
        std::cout << iter << ": New iteration? (y/n) [n] : ";
		
		// Get the string from the user and print both strings 
	    getStringScan(str);
	    printStrings(str, str2);
	    
		// Check if the user entered the string correctly
	    while((str[0] != INT_Y && str[0] != INT_N) || str[1] != INT_NULL){ // Second char must be null
	        // PREVIOUS C code
	        // fprintf(stdout, "Entered value is not correct\nExpecting y or n, got: %s\n", str);
		    // fprintf(stdout, "%10d : New iteration? (y/n) [n] : ", iter); // Not incremented here
	        // EQUIVALENT C++ code
	        std::cout << "Entered value is not correct" << std::endl;
	        std::cout << "Expecting y or n, got: " << str << std::endl;
	        std::cout << iter << " : New iteration? (y/n) [n] : ";
			
			// Get the string from the user and print both strings 
		    getStringScan(str);
		    printStrings(str, str2);
	    }
	    
	iter++;
	
  } while (*str=='y');
  
  // Deallocate the memory for str and str2
  deallocateStrMemory(&str);
  deallocateStrMemory(&str2);

  return 0;
}